$(function () {/*from   w ww .  ja va2 s  . c o  m*/
    function currencyFormat(angka) {
        var rupiah = '';		
        var angkarev = angka.toString().split('').reverse().join('');
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
        return rupiah.split('',rupiah.length-1).reverse().join('');
    }    
    getDateNow = new Date();
    var dateNow = ("0"+getDateNow.getDate()).slice(-2); 
    var monthNow = ("0" + (getDateNow.getMonth() + 1)).slice(-2);  
    var yearNow = getDateNow.getFullYear(); 

    // Grafik Current Production    
    function CurrentProduction(){
        $(".TotalPac").empty("");
        axios.get("https://www.solarkita.com/pvdata/API/InverterData?serial=9947053")
        .then(Response => {
            var TotalPac = (Response.data[0].TotalPac);
            var PVCap = Response.data[0].PVCap;
            $(".TotalPac").append(currencyFormat(parseInt(TotalPac))+" W"); 
            var ctx = document
                .getElementById("currentprod")
                .getContext('2d');
            var gradientFill = ctx.createLinearGradient(0, 0, 0, 450);
            gradientFill.addColorStop(1, "rgba(255, 211, 75,  0.4)");
            gradientFill.addColorStop(1, "rgba(252, 237, 193,   0.4)");
            gradientFill.addColorStop(1, "rgba(252, 237, 193, 0.4)");
            var data = {
                datasets: [
                    {
                        data: [180],
                        fill: false,
                        backgroundColor: [gradientFill],
                        borderWidth: 0
                    }
                ],
                legend: {
                    display: false
                }
            };
            var myDoughnutChart = new Chart(ctx, {
                type: 'gauge',
                data: {
                    labels: ['0', '30.000'],
                    datasets: [{
                        data: [PVCap],
                        value: TotalPac,
                        backgroundColor: "#FFEDB9",
                        borderWidth: 2,
                        label: "CAND. A",
                    }]
                },
                options: {
                    responsive: false,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    layout: {
                        padding: {
                            left: 0,
                            right: 50,
                            top: 0,
                            bottom: 0
                        }
                    },                
                    scales: {
                        YAxes: [
                            {
                                display:false,
                                position: 'right',
                                ticks: {
                                    display: false,
                                    maxTicksLimit: 5,
                                    padding: 0,
                                    color: "red"
                                },
                                gridLines: {
                                    drawTicks: false,
                                    display: 0,
                                    color: 'blue',
                                    circular: true,
                                    lineWidth: 1
                                }
                            }
                        ],
                        xAxes: [{
                            display: true,
                            borderWidth: 0,
                            borderColor: "#fff",
                            gridLines: {
                                drawTicks: true,
                                display: 0,
                                color: '#feecba',
                                circular: true,
                                lineWidth: 1
                            }
    
                        }],
                        // yAxes: [{
                        //     display: true,
                        // 	scaleLabel: {
                        // 		display: false,
                        // 		labelString: 'Value'
                        // 	}
                        // }]                    
                    }
                }
            });
        })
        .catch(error => {
            console.log(error)
        });    
    }
    CurrentProduction();

    // Grafik Total's Production
    function TotalsProduction() {
        axios.get("https://www.solarkita.com/pvdata/API/daydata?serial=9947053&day="+yearNow+"-"+monthNow+"-"+dateNow)
        .then(Response => {
            var ctx_2 = document
                .getElementById("totalprod")
                .getContext('2d');
            var gradientFill = ctx_2.createLinearGradient(0, 0, 0, 450);
            gradientFill.addColorStop(1, "rgba(255, 211, 75, 1)");
            gradientFill.addColorStop(0.2, "rgba(252, 237, 193, 1)");
            gradientFill.addColorStop(1, "rgba(252, 237, 193, 1)");
            // console.log(Response.data[1].TotalYield)

            var data2y = [];
            var data2x = [];

            // console.log(response.data[0].Serial);
            for( var i= 0; i < Response.data.length; i++ ) {
                data2x.push((Response.data[i].Power)/1000)
            }
            for( var i= 0; i < Response.data.length; i++ ) {
                data2y.push(Response.data[i].TimeI)
            }


            var myDoughnutChart_2 = new Chart(ctx_2, {
                type: 'line',
                data: {
                    labels: data2y,
                    datasets: [{
                        label: 'Filled',
                        backgroundColor: gradientFill,
                        borderWidth:0.1,
                        data: data2x,
                        fill: true,
                    }]
                },

                options: {
                    responsive: true,
                    legend:false,
                    title: {
                        display: true,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: false,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'kW'
                            }
                        }]
                    }
                }
            });
        })
        .catch(error => {
            console.log(error)
        });
    }
    TotalsProduction();

    // Grafik Daily
    function GrafikDaily() {
        axios.get("https://www.solarkita.com/pvdata/API/monthdata?serial=9947053&month="+yearNow+"-"+monthNow)
        .then(Response => {

            var data3y = [];
            var data3x = [];
            for( var i= 0; i < Response.data.length; i++ ) {
                data3x.push(Response.data[i].DayYield)
            }
            for( var i= 0; i < Response.data.length; i++ ) {
                data3y.push((Response.data[i].DateI).slice(-2))
            }

            var ctx_3 = document
                .getElementById("daysprod")
                .getContext('2d');
            var gradientFill = ctx_3.createLinearGradient(0, 0, 0, 450);
            gradientFill.addColorStop(1, "rgba(255, 211, 75, 1)");
            gradientFill.addColorStop(0.2, "rgba(252, 237, 193, 1)");
            gradientFill.addColorStop(1, "rgba(252, 237, 193, 1)");
            var data_3 = {
                datasets: [
                    {
                        data: data3x,
                        backgroundColor: gradientFill,
                    }
                ],

            options: { 
                legend: {
                    labels: {
                        fontColor: "white",
                        fontSize: 18
                    }
                }
            },

                labels: data3y
            };
            var myDoughnutChart_3 = new Chart(ctx_3, {
                type: 'bar',
                data: data_3,
                options: {
                    responsive: false,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [
                            { 
                                gridLines: {
                                    drawTicks: false,
                                    display: false,
                                    circular: true,
                                    lineWidth: 1
                                }
                            }
                        ],
                        xAxes: [
                            {
                                ticks: {
                                    maxTicksLimit: 15
                                },
                                scaleLabel: {
                                    show: true,
                                },                            
                                gridLines: {
                                    display: false,
                                }
                            }
                        ]
                    }
                }
            });
        })
        .catch(error => {
            console.log(error)
        });
    }
    GrafikDaily();

    // Grafik Month
    function GrafikMonth() {
        axios.get("https://www.solarkita.com/pvdata/API/yeardata?serial=9947053&year="+yearNow)
        .then(Response => {
            var data4y = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            var data4x = [];
            for( var i= 0; i < 12; i++ ) {
                data4x.push(Response.data[i].TotalYear)
            }
            // for( var i= 0; i < 12; i++ ) {
            //     data4y.push((Response.data[i].MonthI).slice(-2))
            // }        

            var ctx_4 = document
                .getElementById("monthsprod")
                .getContext('2d');
            var gradientFill = ctx_4.createLinearGradient(0, 0, 0, 450);
            gradientFill.addColorStop(1, "rgba(255, 211, 75, 1)");
            gradientFill.addColorStop(0.2, "rgba(252, 237, 193, 1)");
            gradientFill.addColorStop(1, "rgba(252, 237, 193, 1)");

            var data_4 = {
                datasets: [
                    {
                        data: data4x,
                        backgroundColor:gradientFill, 
                    }
                ],
                labels:data4y
            };
            var myDoughnutChart_4 = new Chart(ctx_4, {
                type: 'bar',
                data: data_4,
                options: {
                    responsive: false,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [
                            {
                                gridLines: {
                                    drawTicks: false,
                                    display: false,
                                    circular: true,
                                    lineWidth: 1
                                }
                            }
                        ],
                        xAxes: [
                            {
                                gridLines: {
                                    display: false
                                }
                            }
                        ]
                    }
                }
            });
        })
        .catch(error => {
            console.log(error)
        });
    }
    GrafikMonth();


    // Grafik Year
    function GrafikYear() {
        axios.get("https://solarkita.com/pvdata/API/totaldata?serial=9947053")
        .then(Response => {
            var data5y = [];
            var data5x = [];

            // console.log(response.data[0].Serial);
            for( var i= 0; i < Response.data.length; i++ ) {
                data5x.push(Response.data[i].Total)
            }
            for( var i= 0; i < Response.data.length; i++ ) {
                data5y.push(Response.data[i].YearI)
            }

            var ctx_5 = document
                .getElementById("yearprod")
                .getContext('2d');
            var gradientFill = ctx_5.createLinearGradient(0, 0, 0, 450);
            gradientFill.addColorStop(1, "rgba(255, 211, 75, 1)");
            gradientFill.addColorStop(0.2, "rgba(252, 237, 193, 1)");
            gradientFill.addColorStop(1, "rgba(252, 237, 193, 1)");
            var data_5 = {
                datasets: [
                    {
                        data: data5x,
                        backgroundColor: gradientFill,
                    }
                ],
                labels: data5y
            };
            var myDoughnutChart_5 = new Chart(ctx_5, {
                type: 'bar',
                data: data_5,
                options: {
                    responsive: false,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [
                            {
                                gridLines: {
                                    drawTicks: false,
                                    display: false,
                                    circular: true,
                                    lineWidth: 1
                                }
                            }
                        ],
                        xAxes: [
                            {
                                gridLines: {
                                    display: false
                                }
                            }
                        ]
                    }
                }
            });
        })
        .catch(error => {
            console.log(error)
        });
    }
    GrafikYear();


    // CurrentProduction();
    setInterval(function(){
        CurrentProduction();
        TotalsProduction();
        GrafikDaily();
        GrafikMonth();
        GrafikYear();
    },60000)


    
});
// var ctx1 = document.getElementById("chart1").getContext("2d"); var myChart1 =
// new Chart(ctx1).Line(data,options); var ctx2 =
// document.getElementById("chart2").getContext("2d"); var myChart1 = new
// Chart(ctx2).Line(data,options);