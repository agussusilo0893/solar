$(document).ready(function () {
	var idx_array = [];
	idx_array['summary'] = 0 ;
	idx_array['solarproduction'] = 1 ;
	idx_array['earning'] = 2;
	idx_array['constribution'] = 3;
	idx_array['video'] = 4;


	$("#fullpage").fullpage({
		// normal scroll
		// autoScrolling: false,
		anchors: ["summary", "solarproduction", "earning", "constribution", "video"],
		loopTop: true,
		loopBottom: true,
		fitToSection: false,
		menu: ".mainmenu",
		responsiveWidth: 768,
		afterRender: function () {
			//playing the video
			$('video').get(0).play();
		},
		afterResponsive: function (isResponsive) {
			autoScrolling: true
		}

	});

	$(".closemenu").click(function () {
		$("#headerweb").removeClass("slideshow");
		$("#headerweb").addClass("slidehide");
	});
	$(".sidemenu-mobile .itemmenu").click(function () {
		$("#headerweb").removeClass("slideshow");
		$("#headerweb").addClass("slidehide");
	});

	$(".wrp-showmenu").click(function () {
		$("#headerweb").removeClass("slidehide");
		$("#headerweb").addClass("slideshow");
	})

	$(function () {
		var $anchors = $('.itemmenu');
		var timerdefault = 15000;
		var video = document.getElementById("myVideo");
		
		var click = false;

		(function _loop(idx) {

			var timerGlobal = setTimeout(function () {
				var tam = idx+1; 
				if ( click == true ){
					$anchors.removeClass('active').eq(tam).addClass('active');
				}else{
					$anchors.removeClass('active').eq(idx).addClass('active');
				}
				
				_loop((tam) % $anchors.length);

				if (tam === 0) {
					window.location.replace("#summary");
				}
				else if (tam === 1) {
					window.location.replace("#solarproduction");
				}
				else if (tam === 2) {
					window.location.replace("#earning");
				}
				else if (tam === 3) {
					window.location.replace("#constribution");
				}
				else if (tam === 4) {
					window.location.replace("#video");
					video.pause();
					video.currentTime = 0;
					video.load();
				}
				else {
					window.location.replace("#summary");
				}
			}, timerdefault);

			function timeTime() {
				timerGlobal;
			}

			console.log("IDx "+ idx + "," + timerdefault);

			if (idx+0 === 4) {
				idx = -1;
				timerGlobal;
			}
			if ( idx == 3 ){
				video.play();
				idx = 3;
				timerdefault = (video.duration) * 1000;
				timerGlobal;
			}

			if ( idx <= 0 ){
				timerdefault = 15000;
				timerGlobal;
			}

			$(".itemmenu").click(function(){
				click = true;
				idx = idx_array[$(this).data("menuanchor")]  ;
				console.log(idx + "," + $(this).data("menuanchor"));
				clearTimeout(timerGlobal);
			});
			
		}(0));

		
	});


});




