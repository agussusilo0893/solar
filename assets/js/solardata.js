$(document).ready(function () {
    function currencyFormat(angka) {
        var rupiah = '';		
        var angkarev = angka.toString().split('').reverse().join('');
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
        return rupiah.split('',rupiah.length-1).reverse().join('');
    }    

        function dataGLobalSolar() {
            $(".valtotalprod").empty("");
            $(".valtotalearning").empty("");
            $(".valtotalco2").empty("");
            $(".valtotalearningtoday").empty("");
            $(".valtotalearningmonth").empty("");
            $(".valtotalearningyear").empty("");
            $(".valTreeTotal").empty("");
            $(".valElecCarTotal").empty("");
            $(".valConsumptionTotal").empty("");
            $(".TotalDays").empty("");
            axios.get("https://www.solarkita.com/pvdata/API/InverterDataSum?serial=9947053")
            .then(Response => {
                var valtotalprod = (Response.data[0].ETotal);
                var valtotalearning = Response.data[0].BillTotal;
                var valtotalco2 = Response.data[0].CO2Total;
                var valtotalearningtoday = Response.data[0].BillToday;
                var valtotalearningmonth = Response.data[0].BillThisMonth;
                var valtotalearningyear = Response.data[0].BillThisYear;
                var valTreeTotal = Response.data[0].TreeTotal;
                var valElecCarTotal = Response.data[0].EVTotal;
                var valConsumptionTotal = Response.data[0].CoalTotal;
                var valEToday = Response.data[0].EToday;
                $(".valtotalprod").append(currencyFormat((parseInt(valtotalprod)))+" kWh");                
                $(".valtotalearning").append("Rp. "+currencyFormat(parseInt(valtotalearning)))
                $(".valtotalco2").append(currencyFormat(valtotalco2)+" kg");
                $(".valtotalearningtoday").append("Rp. "+currencyFormat(valtotalearningtoday));
                $(".valtotalearningmonth").append("Rp. "+currencyFormat(valtotalearningmonth));
                $(".valtotalearningyear").append("Rp. "+currencyFormat(valtotalearningyear));
                $(".valTreeTotal").append(currencyFormat(valTreeTotal)+" Trees");
                $(".valElecCarTotal").append(currencyFormat(valElecCarTotal) +" km");
                $(".valConsumptionTotal").append(currencyFormat(valConsumptionTotal)+" kg");
                $(".TotalDays").append(valEToday+" kWh");
            })
            .catch(error => {
                console.log(error)
            });
        }
        dataGLobalSolar();
        setInterval(function(){            
            dataGLobalSolar();            
        },60000)

});