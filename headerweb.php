<div id="headerweb">
	<div class="wrp-sidemenu">
		<a href="#" class="itemsidebar mainlogo"><img src="<?php echo $baseurl; ?>assets/images/logo.svg" alt=""/></a>
		<div class="itemsidebar mainmenu">
			<a href="<?php echo $baseurl; ?>#summary" class="summarypage active">
				<div class="imgwrp"><?php include "assets/images/icon_summary.svg" ?></div>
				<label>SUMMARY</label>
			</a>
			<a href="<?php echo $baseurl; ?>#solarproduction" class="solarproductionpage">
				<div class="imgwrp"><?php include "assets/images/icon_solarproduction.svg" ?></div>
				<label>SOLAR PRODUCTION</label>
			</a>
			<a href="<?php echo $baseurl; ?>#earning" class="earningpage">
				<div class="imgwrp"><?php include "assets/images/icon_earning.svg" ?></div>
				<label>EARNINGS</label>
			</a>
			<a href="<?php echo $baseurl; ?>#constribution" class="contributionspage">
				<div class="imgwrp"><?php include "assets/images/icon_contribution.svg" ?></div>
				<label>CONTRIBUTIONS</label>
			</a>
			<a href="<?php echo $baseurl; ?>#video" class="videopage">
				<div class="imgwrp"><?php include "assets/images/icon_video.svg" ?></div>
				<label>VIDEO</label>
			</a>
		</div>
	</div>
</div>
