<?php include "global.php" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>..:: Web Development ::..</title>
<meta name="description" content="Description page here"/>
<meta name="keywords" content="Frontend, Website, HTML, CSS"/>
<meta name="author" content="Developer"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link rel="shortcut icon" href="assets/images/fav.png"/>
<meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
<link rel="stylesheet" type="text/css" href="<?php echo $baseurl;?>assets/css/main.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $baseurl;?>assets/css/custom.css"/>
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="assets/css/IE8styles.css"/>
<![endif]-->
<script type="text/javascript" src="<?php echo $baseurl;?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $baseurl;?>assets/js/wow.min.js"></script>
<script type="text/javascript" src="<?php echo $baseurl;?>assets/js/slick.min.js"></script>
<script type="text/javascript" src="<?php echo $baseurl;?>assets/js/weather.js"></script>
<script type="text/javascript" src="<?php echo $baseurl;?>assets/js/axios.min.js"></script>
<script type="text/javascript" src="<?php echo $baseurl;?>assets/js/solardata.js"></script>
<script type="text/javascript" src="<?php echo $baseurl;?>assets/js/jquery.fullPage.js"></script>
<script>
	if (screen && screen.width > 768) {
		document.write('');
	}
</script>

<script type="text/javascript" src="<?php echo $baseurl;?>assets/js/Chart.bundle.js"></script>
<script type="text/javascript" src="<?php echo $baseurl;?>assets/js/chartjs-gauge.js"></script>
<script type="text/javascript" src="<?php echo $baseurl;?>assets/js/utils.js"></script>

<script type="text/javascript" src="<?php echo $baseurl;?>assets/js/myjs.js"></script>
<script type="text/javascript" src="<?php echo $baseurl;?>assets/js/datachart.js"></script>

</head>
<body>

<div id="headerweb">
	<a class="closemenu">
		close <i class="fa fa-times"></i>
	</a>
	<div class="wrp-sidemenu">
		<a href="<?php echo $baseurl;?>" class="itemsidebar mainlogo"><img src="<?php echo $baseurl; ?>assets/images/logo.png" alt=""/></a>
		<div class="itemsidebar mainmenu">
			<div class="itemmenu" data-menuanchor="summary">
				<a href="<?php echo $baseurl; ?>#summary">
					<div class="imgwrp"><img src="<?php echo $baseurl; ?>assets/images/icon_summary.png" alt=""/></div>
					<label>SUMMARY</label>
				</a>
			</div>

			<div class="itemmenu" data-menuanchor="solarproduction">
				<a href="<?php echo $baseurl; ?>#solarproduction">
					<div class="imgwrp"><img src="<?php echo $baseurl; ?>assets/images/icon_solarproduction.png" alt=""/></div>
					<label>SOLAR PRODUCTION</label>
				</a>
			</div>

			<div class="itemmenu" data-menuanchor="earning">
				<a href="<?php echo $baseurl; ?>#earning">
					<div class="imgwrp"><img src="<?php echo $baseurl; ?>assets/images/icon_earning.png" alt=""/></div>
					<label>EARNINGS</label>
				</a>
			</div>

			<div class="itemmenu" data-menuanchor="constribution">
				<a href="<?php echo $baseurl; ?>#constribution">
					<div class="imgwrp"><img src="<?php echo $baseurl; ?>assets/images/icon_contribution.png" alt=""/></div>
					<label>CONTRIBUTIONS</label>
				</a>
			</div>

			<div class="itemmenu" data-menuanchor="video">
				<a href="<?php echo $baseurl; ?>#video">
					<div class="imgwrp"><img src="<?php echo $baseurl; ?>assets/images/icon_video.png" alt=""/></div>
					<label>VIDEO</label>
				</a>
			</div>
		</div>
	</div>

	<div class="wrp-sidemenu sidemenu-mobile">
		<a href="<?php echo $baseurl;?>" class="itemsidebar mainlogo"><img src="<?php echo $baseurl; ?>assets/images/logo.png" alt=""/></a>
		<div class="itemsidebar mainmenu">
			<div class="itemmenu" data-menuanchor="summary">
				<a href="<?php echo $baseurl; ?>#summary">
					<div class="imgwrp"><img src="<?php echo $baseurl; ?>assets/images/icon_summary.png" alt=""/></div>
					<label>SUMMARY</label>
				</a>
			</div>

			<div class="itemmenu" data-menuanchor="solarproduction">
				<a href="<?php echo $baseurl; ?>#solarproduction">
					<div class="imgwrp"><img src="<?php echo $baseurl; ?>assets/images/icon_solarproduction.png" alt=""/></div>
					<label>SOLAR PRODUCTION</label>
				</a>
			</div>

			<div class="itemmenu" data-menuanchor="earning">
				<a href="<?php echo $baseurl; ?>#earning">
					<div class="imgwrp"><img src="<?php echo $baseurl; ?>assets/images/icon_earning.png" alt=""/></div>
					<label>EARNINGS</label>
				</a>
			</div>

			<div class="itemmenu" data-menuanchor="constribution">
				<a href="<?php echo $baseurl; ?>#constribution">
					<div class="imgwrp"><img src="<?php echo $baseurl; ?>assets/images/icon_contribution.png" alt=""/></div>
					<label>CONTRIBUTIONS</label>
				</a>
			</div>

			<div class="itemmenu" data-menuanchor="video">
				<a href="<?php echo $baseurl; ?>#video">
					<div class="imgwrp"><img src="<?php echo $baseurl; ?>assets/images/icon_video.png" alt=""/></div>
					<label>VIDEO</label>
				</a>
			</div>
		</div>
	</div>

</div>

<div class="headermobile">
	<div class="wrp-showmenu">
		<span></span>
		<span></span>
		<span></span>
	</div>
	<a href="<?php echo $baseurl;?>" class="logomobile"><img src="<?php echo $baseurl; ?>assets/images/logo-mobile.png" alt=""/></a>
</div>

<div id="bodyweb">
	<div id="fullpage">
		<div class="section section-summary" data-anchor="summary">
			<div class="container-itemsummary">
				<div class="wrp-itemsummary">
					<div class="item-summary">
						<div class="content-summary">
							<div class="imgwrp img-summary-1" style="width:50px"><?php include "assets/images/icon_totalproduction.svg"?></div>
							<h4>TOTAL PRODUCTION</h4>
							<h2 class="valtotalprod"></h2>
						</div>
					</div>
					<div class="item-summary">
						<div class="content-summary">
							<div class="imgwrp img-summary-2"><?php include "assets/images/icon_totalearning.svg"?></div>
							<h4>TOTAL EARNINGS</h4>
							<h2 class="valtotalearning"></h2>
						</div>
					</div>
				</div>
				<div class="wrp-itemsummary d-xs-none d-sm-none d-xl-flex d-lg-flex d-md-flex">
					<div class="item-summary">
						<div class="content-summary img-summary-3">
							<div class="imgwrp"><?php include "assets/images/icon_totalco.svg"?></div>
							<h4>TOTAL CO<sub>2</sub> REDUCED</h4>
							<h2 class="valtotalco2"></h2>
						</div>
					</div>
					<div class="item-summary">
						<div class="content-summary">
							<div class="mainweather">
								<h4>WEATHER</h4>
								<div class="wrp-weather">
									<div class="left">
										<i id="weather-icon" class="wi wi-fw weather-icon wi-day-sunny"></i>
									</div>
									<div class="right">
										<div class="weather-status">
											<div class="wrp-temp"><div id="temperature"> </div><sup>o</sup>c</div>
											<div id="city" class="city">Kota</div>
											<div class="wrp-desc">
												Description
												<div id="forcast" class="forcast">Sky Is Clear</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="section section-solar-production" data-anchor="solarproduction">
			<div class="wrp-defaultpage">
				<div class="wrp-sectionpage" style="height:auto">
					<div class="item-sectionpage" style="height:auto">
						<div class="maintitle">
							<h1>SOLAR PRODUCTION</h1>
						</div>
					</div>
				</div>

				<div class="wrp-sectionpage section-items">
					<div class="item-sectionpage itemsection-chart d-xs-none d-sm-none d-xl-block d-lg-block d-md-block">
						
						<canvas id="currentprod" class="itemchart"></canvas>

						<div class="infochart">
							<h5>&nbsp;</h5>
						</div>
						<div class="infochart totop">
							<h5>Current Production</h5>
							<h2 class="TotalPac"></h2>
						</div>
					</div>
					<div class="item-sectionpage item-sectionpage-todays item-totalprod itemsection-chart">
						<canvas id="totalprod" class="itemchart"></canvas>
						<div class="infochart totop position-totalDays">
							<h5>Today’s Production</h5>
							<h2 class="TotalDays"></h2>
						</div>
						<div class="separator-line"></div>						
					</div>
				</div>
				<div class="wrp-sectionpage section-items mm-24">
					<div class="item-sectionpage item-dailykwh itemsection-chart">
						<canvas id="daysprod" class="itemchart"></canvas>
						<div class="infochart-graphic-bar">
							<h5>Daily (kWh)</h5>
						</div>
					</div>
					<div class="item-sectionpage itemsection-chart d-xs-none d-sm-none d-xl-block d-lg-block d-md-block">
						<canvas id="monthsprod" class="itemchart"></canvas>
						<div class="infochart-graphic-bar">
							<h5>Monthly (kWh)</h5>
						</div>
					</div>
					<div class="item-sectionpage itemsection-chart d-xs-none d-sm-none d-xl-block d-lg-block d-md-block">
						<canvas id="yearprod" class="itemchart"></canvas>
						<div class="infochart-graphic-bar">
							<h5>Yearly (kWh)</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="section section-earning"  data-anchor="earning">
			<div class="wrp-defaultpage">
				<div class="wrp-sectionpage d-xs-none d-sm-none d-xl-block d-lg-block d-md-block">
					<div class="item-sectionpage">
						<div class="maintitle mb-32">
							<h1>TOTAL EARNINGS</h1>
							<h5>*Based on PLN energy rate</h5>
						</div>
						<div class="imgwrp imgiconearning-main"><img src="<?php echo $baseurl; ?>assets/images/icon_totalearning.png" alt=""/></div>
						<h2 class="valtotalearning"></h2>
					</div>
				</div>
				<div class="wrp-sectionpage section-items ">
					<div class="item-sectionpage item-section-earn">
						<div class="maintitle d-block d-xs-block d-sm-block d-xl-block d-lg-block d-md-block">
							<div class="maintitle mb-32 d-xl-none d-lg-none d-md-none d-sm-block d-xs-block">
								<h1>TOTAL EARNINGS</h1>
							</div>								
							<h2 class="sametitle">Today</h2>
						</div>
						<div class="imgwrp imgiconearning"><img src="<?php echo $baseurl; ?>assets/images/icon_earningtoday.png" alt=""/></div>
						<h4 class="valtotalearningtoday"></h4>
					</div>
					<div class="item-sectionpage item-section-earn">
						<div class="maintitle">
							<h2 class="sametitle">This Month</h2>
						</div>
						<div class="imgwrp imgiconearning"><img src="<?php echo $baseurl; ?>assets/images/icon_totalmonth.png" alt=""/></div>

						<h4 class="valtotalearningmonth"></h4>
					</div>
					<div class="item-sectionpage item-section-earn d-xs-none d-sm-none d-xl-block d-lg-block d-md-block">
						<div class="maintitle">
							<h2 class="sametitle">This Year</h2>
						</div>
						<div class="imgwrp imgiconearning"><img src="<?php echo $baseurl; ?>assets/images/icon_totalyear.png" alt=""/></div>

						<h4 class="valtotalearningyear"></h4>
					</div>
				</div>
			</div>
		</div>
		<div class="section section-constribution"  data-anchor="constribution">
			<div class="wrp-defaultpage">
				<div class="wrp-sectionpage">
					<div class="item-sectionpage item-section-co2">
						<div class="maintitle d-xs-none d-sm-none d-xl-block d-lg-block d-md-block">
							<h1>TOTAL CO<sub>2</sub> REDUCED</h1>
						</div>
						<div class="maintitle d-block d-xs-block d-sm-block d-xl-none d-lg-none d-md-none">
							<h1>CONTRIBUTIONS</h1>
						</div>						
						<div class="imgwrp imgco2"><?php include "assets/images/icon_co2.svg"; ?></div>
						<h2 class="valtotalco2"></h2>
						<h5 class="equal">Equals to:</h5>
					</div>
				</div>

				<div class="wrp-sectionpage section-items section-reduced">
					<div class="item-sectionpage item-section-reduced d-xs-none d-sm-none d-xl-flex d-lg-flex d-md-flex">
						<div class="imgwrp img-car"><?php include "assets/images/icon_car.svg"; ?></div>
						<h5>Electric Car</h5>
						<h4 class="valElecCarTotal"></h4>
					</div>
					<div class="item-sectionpage item-section-reduced d-xs-none d-sm-none d-xl-flex d-lg-flex d-md-flex">
						<div class="imgwrp img-coal"><?php include "assets/images/icon_coal.svg"; ?></div>
						<h5>Coal Consumption Reduced</h5>
						<h4 class="valConsumptionTotal"></h4>
					</div>
					<div class="item-sectionpage item-section-reduced">
						<div class="imgwrp imgtree"><?php include "assets/images/icon_tree.svg"; ?></div>
						<h5>Total Tree Planted</h5>
						<h4 class="valTreeTotal"></h4>
					</div>
				</div>
			</div>
		</div>
		<div class="section section-video"  data-anchor="slidvideoe1">
			<div class="wrp-video">
				<div class="contentvideo">
					<video   autoplay="true" muted="false" controls="true"  id="myVideo">
						<source src="<?php echo $baseurl;?>assets/images/video.mp4" type="video/mp4">
						<!-- <source src="<?php echo $baseurl;?>assets/images/video.webm" type="video/webm"> -->
					</video>
				</div>
			</div>
		</div>
	</div>
</div><!-- endbodyweb -->

<div id="footerweb">
	<div class="contentfoot">
		<div class="item"> Solar System Capacity: <strong>29,700 Watt Peak</strong></div>
		<div class="item"> Location: <strong>Jakarta Cathedral</strong></div>
	</div>
</div><!-- endfooterweb -->


</body>
</html>
